# Capstone for Python Programming
from abc import ABC, abstractmethod


class Person(ABC):

    @abstractmethod
    def get_fullname(self):
        pass

    @abstractmethod
    def add_request(self):
        pass

    @abstractmethod
    def check_request(self):
        pass

    @abstractmethod
    def add_user(self):
        pass


# 2. Create an Employee class that inherits from the Person class
class Employee(Person):

    def __repr__(self):
        return f"Employee({self._first_name}, {self._last_name}, {self._email}, {self._department})"

    def __str__(self):
        return f"Employee({self._first_name}, {self._last_name}, {self._email}, {self._department})"

    def __init__(self, first_name, last_name, email, department):
        super().__init__()

        self._first_name = first_name
        self._last_name = last_name
        self._email = email
        self._department = department
        self._requests = []

    # getter and setter for first_name
    def get_first_name(self):
        print(f"First name: {self._first_name}")

    def set_first_name(self, first_name):
        self._first_name = first_name

    # getter and setter for last_name
    def get_last_name(self):
        print(f"Last name: {self._last_name}")

    def set_last_name(self, last_name):
        self._last_name = last_name

    # getter and setter for email
    def get_email(self):
        print(f"Email: {self._email}")

    def set_email(self, email):
        self._email = email

    # getter and setter for department
    def get_department(self):
        print(f"Department: {self._department}")

    def set_department(self, department):
        self._department = department

    # override abstract methods
    def get_fullname(self):
        return f"{self._first_name} {self._last_name}"

    def add_request(self):
        return "Request added"

    def check_request(self):
        pass

    def add_user(self):
        pass

    # other methods
    def login(self):
        return f"{self._email} is logged in"

    def logout(self):
        return f"{self._email} is logged out"


# 3. Create a TeamLead class that inherits from the Person class

class TeamLead(Person):
    def __init__(self, first_name, last_name, email, department):
        super().__init__()

        self._first_name = first_name
        self._last_name = last_name
        self._email = email
        self._department = department
        self._members = []

    # getter and setter for first_name
    def get_first_name(self):
        print(f"First name: {self._first_name}")

    def set_first_name(self, first_name):
        self._first_name = first_name

    # getter and setter for last_name
    def get_last_name(self):
        print(f"Last name: {self._last_name}")

    def set_last_name(self, last_name):
        self._last_name = last_name

    # getter and setter for email
    def get_email(self):
        print(f"Email: {self._email}")

    def set_email(self, email):
        self._email = email

    # getter and setter for department
    def get_department(self):
        print(f"Department: {self._department}")

    def set_department(self, department):
        self._department = department

    # override abstract methods
    def get_fullname(self):
        return f"{self._first_name} {self._last_name}"

    def add_request(self):
        pass

    def check_request(self):
        pass

    def add_user(self):
        pass

    # other methods
    def login(self):
        return f"{self._email} is logged in"

    def logout(self):
        return f"{self._email} is logged out"

    def add_member(self, member):
        self._members.append(member)

    def remove_member(self, member):
        self._members.remove(member)

    def get_members(self):
        # return f"Members: {self._members}"
    
        return self._members


# 4. Create an Admin class that inherits from the Person class

class Admin(Person):
    def __init__(self, first_name, last_name, email, department):
        super().__init__()

        self._first_name = first_name
        self._last_name = last_name
        self._email = email
        self._department = department
        self._members = []

    # getter and setter for first_name
    def get_first_name(self):
        print(f"First name: {self._first_name}")

    def set_first_name(self, first_name):
        self._first_name = first_name

    # getter and setter for last_name
    def get_last_name(self):
        print(f"Last name: {self._last_name}")

    def set_last_name(self, last_name):
        self._last_name = last_name

    # getter and setter for email
    def get_email(self):
        print(f"Email: {self._email}")

    def set_email(self, email):
        self._email = email

    # getter and setter for department
    def get_department(self):
        print(f"Department: {self._department}")

    def set_department(self, department):
        self._department = department

    # override abstract methods
    def get_fullname(self):
        return f"{self._first_name} {self._last_name}"

    def add_request(self):
        pass

    def check_request(self):
        pass

    def add_user(self):
        return "New User added"

    # other methods
    def login(self):
        return f"{self._email} is logged in"

    def logout(self):
        return f"{self._email} is logged out"

    def add_member(self, member):
        self._members.append(member)

    def remove_member(self, member):
        self._members.remove(member)

    def get_members(self):
        print(f"Members: {self._members}")


# 5. Create a Request class

class Request:
    def __init__(self, name, requestor, date_requested):
        self._name = name
        self._requestor = requestor
        self._date_requested = date_requested
        self._status = "Open"

    def update_request(self):
        print(f"{self._name} has been updated")

    def close_request(self):
        # print(f"{self._name} has been closed")

        return f"{self._name} has been closed"

    def cancel_request(self):
        print(f"{self._name} has been cancelled")

    def set_status(self, status):
        self._status = status

    def get_status(self):
        print(f"Status: {self._status}")


# test cases

employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")


# assert statements
assert employee1.get_fullname() == "John Doe", "Full name should be John Doe"
assert admin1.get_fullname() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.get_fullname() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com is logged in"
assert employee2.add_request() == "Request added"
assert employee2.logout() == "sjane@mail.com is logged out"


teamLead1.add_member(employee3)
teamLead1.add_member(employee4)

for indiv_emp in teamLead1.get_members():
    print(indiv_emp.get_fullname())

# assert admin1.add_user() == "New User added"
#
# req2.set_status("closed")
# print(req2.close_request())













