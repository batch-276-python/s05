from activity import Employee, Admin, TeamLead


def test_get_employee_full_name():
    employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
    admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
    team_lead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")

    assert employee1.get_fullname() == "John Doe"
    assert admin1.get_fullname() == "Monika Justin"
    assert team_lead1.get_fullname() == "Michael Specter"


def test_employee_login():
    employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")

    assert employee2.login() == "sjane@mail.com is logged in"


def test_employee_logout():
    employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")

    assert employee3.logout() == "probert@mail.com is logged out"
